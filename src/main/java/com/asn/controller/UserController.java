package com.asn.controller;

import com.asn.domain.UserDTO;
import com.asn.entity.AsnUser;
import com.asn.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/asn/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AsnUser register(@RequestBody UserDTO userDTO) {
        return userService.register(userDTO);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<UserDTO> listUser(){
        return userService.findAll();
    }
}
