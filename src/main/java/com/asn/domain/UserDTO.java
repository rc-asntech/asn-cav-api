package com.asn.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO {
    private String userName;
    private String password;
    private String email;
    private String mobile;
}
