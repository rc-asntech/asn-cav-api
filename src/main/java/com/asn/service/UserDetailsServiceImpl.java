package com.asn.service;

import com.asn.entity.AsnUser;
import com.asn.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) {
        Optional<AsnUser> optionalAsnUser = userRepository.findByUserName(userName);
        if (optionalAsnUser.isPresent()) {
            AsnUser asnUser = optionalAsnUser.get();
            List<GrantedAuthority> authorities = asnUser.getRoles()
                    .stream().map(roles -> new SimpleGrantedAuthority(roles.getRoleName()))
                    .collect(Collectors.toList());
            return new User(asnUser.getUserName(), asnUser.getPassword(), authorities);
        }
        throw new RuntimeException("User Not Exist");
    }
}
