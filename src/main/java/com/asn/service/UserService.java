package com.asn.service;

import com.asn.domain.UserDTO;
import com.asn.entity.AsnUser;
import com.asn.mapper.MapStructMapper;
import com.asn.repository.UserRepository;
import com.asn.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MapStructMapper mapStructMapper;

    /**
     *
     * @param userDTO
     * @return
     */
    public AsnUser register(UserDTO userDTO) {
        AsnUser asnUser = mapStructMapper.userDTOToAsnUser(userDTO);
        asnUser.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        asnUser.setRoles(Collections.singletonList(userRoleRepository.findByRoleNameContaining("USER")));
        Optional<AsnUser> optUser = userRepository.findByUserNameOrEmail(userDTO.getUserName(), userDTO.getEmail());
        if (!optUser.isPresent()) {
            return userRepository.save(asnUser);
        }
        throw new RuntimeException("User Already Exist");
    }

    /**
     *
     * @return
     */
    public List<UserDTO> findAll(){
        return mapStructMapper.usersToUserDTOList(userRepository.findAll());
    }
}
