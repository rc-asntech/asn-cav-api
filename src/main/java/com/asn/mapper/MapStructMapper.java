package com.asn.mapper;

import com.asn.domain.UserDTO;
import com.asn.entity.AsnUser;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapStructMapper {

    UserDTO asnUserToUserDTO(AsnUser asnUser);

    List<UserDTO> usersToUserDTOList(List<AsnUser> asnUsers);

    AsnUser userDTOToAsnUser(UserDTO userDTO);

}
