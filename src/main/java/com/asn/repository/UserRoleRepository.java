package com.asn.repository;

import com.asn.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleRepository extends JpaRepository<Role, Long> {

    Role findByRoleNameContaining(String roleName);

}
