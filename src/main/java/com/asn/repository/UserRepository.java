package com.asn.repository;

import com.asn.entity.AsnUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<AsnUser, Long> {

    Optional<AsnUser> findByUserName(String username);

    Optional<AsnUser> findByUserNameOrEmail(String username, String email);
}
