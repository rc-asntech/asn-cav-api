package com.asn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.asn"})
public class AsnBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsnBackendApplication.class, args);
	}

}
