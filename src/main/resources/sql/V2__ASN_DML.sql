-- ############################################################################################# --

-- BASIC AUTH with token time declarations_role
-- ACCESS TOKEN VALIDITY = 300 SECOND
-- REFRESH TOKEN VALIDITY = 1800 SECOND
-- insert client details [clientId = asn-client-id & clientSecret = asn-client@secret321]

INSERT INTO oauth_client_details (client_id, client_secret, scope, authorized_grant_types, authorities, access_token_validity, refresh_token_validity)
VALUES ('asn-client-id', '$2a$04$gkHFLRAwhGoqgOfzaSDAkud7jNf0iW4xecIW35xoID21RQ.Kx8nhS', 'read,write', 'password,refresh_token,client_credentials,authorization_code', 'ROLE_TRUSTED_CLIENT', 300, 1800);

-- ##########################################################--
-- USER ROLES

INSERT INTO role(id, role_name, description)
VALUES (1, 'ROLE_ADMIN', 'Admin User - Has permission to perform admin tasks');

INSERT INTO role(id, role_name, description)
VALUES (2, 'ROLE_USER', 'CONSULTANT - Has no admin rights');